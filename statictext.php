<?php
/**
* 2019 Aleh Mialeshka
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Aleh Mialeshka (aleh.mialeshka90@gmail.com)
*  @copyright  2019 aleh.mialeshka90@gmail.com

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class StaticText extends Module
{
    public function __construct()
    {
        $this->name = 'statictext';
        $this->tab = 'others';
        $this->version = '1.0.0';
        $this->author = 'Aleh Mialeshka';
        $this->ps_version_compliancy = ['min' => '1.6', 'max' => _PS_VERSION_];
        $this->bootstrap = true;
        parent::__construct();
        $this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('Static Text');
        $this->description = $this->l('The block with static text');
    }

    public function install()
    {
        return parent::install() && $this->registerHook('displayHome') && $this->registerHook('displayHeader');
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function hookHome($params)
    {
        return $this->display(__FILE__, '/views/templates/front/statictext.tpl');
    }
	
	 public function hookHeader()
    {
        $this->context->controller->addJquery();
		$this->context->controller->registerStylesheet(
			"statictextcss",
			'modules/' . $this->name . '/views/css/style.css',
			array('server' => 'local', 'priority' => 150)
		);
		$this->context->controller->registerJavascript(
			"statictextjs",
			'modules/' . $this->name . '/views/js/front.js',
			array('server' => 'local', 'priority' => 150)
		);
    }
}
